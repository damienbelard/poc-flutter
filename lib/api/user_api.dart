import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:poc_flutter/model/User.dart';

class UserApi {}

Future<List<User>> fetchUserList() async {
  return await http.get('https://jsonplaceholder.typicode.com/posts').then(
      (response) => response.statusCode == 200
          ? (User.fromMapList(json.decode(response.body)))
          : (throw Exception('Failed to load post')));
}

Future<http.Response> fetch(String url) async {
  return await http.get(url);
}
