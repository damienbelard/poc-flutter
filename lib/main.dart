import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:poc_flutter/theme/PlatformTheme.dart';
import 'package:poc_flutter/widget/AnimationDemo.dart';
import 'package:poc_flutter/widget/Chat.dart';
import 'package:poc_flutter/widget/MyApps.dart';
import 'package:poc_flutter/widget/RandomWords.dart';
import 'package:poc_flutter/widget/user/UserList.dart';

void main() => runApp(MaterialApp(
    title: 'POC Flutter',
    theme:
        defaultTargetPlatform == TargetPlatform.iOS ? kIOSTheme : kDefaultTheme,
    initialRoute: 'home',
    routes: {
      'home': (context) => MyApps(),
      'list': (context) => RandomWords(),
      'chat': (context) => ChatScreen(),
      'users': (context) => News(),
      'animation': (context) => AnimationWidget()
    },
    home: MyApps()));
