import 'package:flutter/material.dart';
import 'package:poc_flutter/api/user_api.dart';
import 'package:poc_flutter/model/User.dart';
import 'package:poc_flutter/widget/user/UserDetails.dart';

class NewsState extends State<News> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text('Users'),
          elevation:
          Theme
              .of(context)
              .platform == TargetPlatform.iOS ? 0.0 : 4.0,
          backgroundColor: Colors.indigo,
        ),
        body: FutureBuilder<List<User>>(
          future: fetchUserList(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return ListView.builder(
                padding: EdgeInsets.all(8.0),
                itemCount: snapshot.data.length,
                itemBuilder: (BuildContext context, int index) {
                  return new NewWidget(snapshot.data, index);
                },
              );
            } else if (snapshot.hasError) {
              return Text("${snapshot.error}");
            }

            return new Container(
              child: new CircularProgressIndicator(),
              alignment: Alignment.center,
            );
          },
        ));
  }
}

class NewWidget extends StatelessWidget {
  const NewWidget(this.user,
      this.index, {
        Key key,
      }) : super(key: key);

  final List<User> user;
  final int index;

  @override
  Widget build(BuildContext context) {
    var src =
        "https://cdn-images-1.medium.com/max/1200/1*5-aoK8IBmXve5whBQM90GA.png";
    return Card(
      child: FlatButton(
        onPressed: () {
          Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => UserDetailWidget(user.elementAt(index))));
        },
        child: Row(
          children: <Widget>[
            Container(
              width: 100,
              child: Hero(
                tag: 'image' + user
                    .elementAt(index)
                    .id
                    .toString(),
                child: Image.network(
                  src,
                ),
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 8, 16, 8),
                child: Column(
                  children: <Widget>[
                    Text(
                      user
                          .elementAt(index)
                          .title
                          .toString(),
                      style: Theme
                          .of(context)
                          .textTheme
                          .title,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8),
                      child: Text(
                        user
                            .elementAt(index)
                            .body
                            .toString(),
                        style: Theme
                            .of(context)
                            .textTheme
                            .subtitle,
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class News extends StatefulWidget {
  @override
  NewsState createState() => NewsState();
}
