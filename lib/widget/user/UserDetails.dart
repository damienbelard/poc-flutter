import 'package:flutter/material.dart';
import 'package:poc_flutter/model/User.dart';

class UserDetailWidget extends StatelessWidget {
  const UserDetailWidget(
    this.user, {
    Key key,
  }) : super(key: key);

  final User user;

  @override
  Widget build(BuildContext context) {
    var src =
        "https://cdn-images-1.medium.com/max/1200/1*5-aoK8IBmXve5whBQM90GA.png";
    return Scaffold(
      appBar: AppBar(
        title: Text(user.id.toString()),
        elevation: Theme.of(context).platform == TargetPlatform.iOS ? 0.0 : 4.0,
        backgroundColor: Colors.indigo,
      ),
      body: Column(
        children: <Widget>[
          Container(
            width: 100,
            child: Padding(
              padding: const EdgeInsets.only(top: 8),
              child: Hero(
                tag: 'image' + user.id.toString(),
                child: Image.network(
                  src,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 16, bottom: 8),
            child: Text(
              "Title: ",
              style: Theme.of(context).textTheme.title,
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Text(
                user.title,
                style: Theme
                    .of(context)
                    .textTheme
                    .subtitle,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 16, bottom: 8),
            child: Text(
              "Body: ",
              style: Theme.of(context).textTheme.title,
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Text(
                user.body,
                style: Theme.of(context).textTheme.subtitle,
              ),
            ),
          )
        ],
      ),
    );
  }
}
