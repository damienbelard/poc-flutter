import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class AnimationWidget extends StatefulWidget {
  AnimationWidgetState createState() => AnimationWidgetState();
}

class AnimationWidgetState extends State<AnimationWidget>
    with SingleTickerProviderStateMixin {
  AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 2000));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Animations"),
          elevation:
              Theme.of(context).platform == TargetPlatform.iOS ? 0.0 : 4.0,
          backgroundColor: Colors.indigo,
        ),
        body: Row(
          children: <Widget>[
            Expanded(
              child: Container(
                alignment: Alignment.center,
                child: VerticalSwpieGesture(Column(
                  children: <Widget>[
                    AnimatedLogo(controller: controller),
                  ],
                )),
                padding: EdgeInsets.only(bottom: 8),
              ),
            ),
          ],
        ));
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  Widget VerticalSwpieGesture(Widget body) {
    return GestureDetector(
        onVerticalDragEnd: (details) {
          if (details.primaryVelocity > 0) {
            controller.forward();
          }
          if (details.primaryVelocity < 0) {
            controller.reverse();
          }
        },
        child: body);
  }
}

class AnimatedLogo extends AnimatedWidget {
  AnimatedLogo({Key key, this.controller})
      : super(key: key, listenable: controller);

  final AnimationController controller;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Image.asset(
        "assets/logo.png",
        //width: 100,
        width: size(),
        //height: 100,
        height: size(),
        alignment: alignment(),
      ),
    );
  }

  double opacity() {
    return Tween<double>(begin: 0, end: 1)
        .animate(CurvedAnimation(
            parent: controller,
            curve: Interval(0, 1, curve: Curves.fastOutSlowIn)))
        .value;
  }

  double size() {
    return Tween<double>(begin: 100, end: 300)
        .animate(CurvedAnimation(parent: controller, curve: Interval(0.0, 0.5)))
        .value;
  }

  Alignment alignment() {
    return Tween<Alignment>(
            begin: Alignment.center, end: Alignment.bottomCenter)
        .animate(
            CurvedAnimation(parent: controller, curve: Interval(0.5, 0.75)))
        .value;
  }
}
