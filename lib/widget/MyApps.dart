import 'package:flutter/material.dart';
import 'package:poc_flutter/model/App.dart';

class MyApps extends StatelessWidget {
  final _apps = [
    new App('List', 'list'),
    new App('Chat', 'chat'),
    new App('Users', 'users'),
    new App('Animation', 'animation')
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('POCs'),
        elevation: Theme.of(context).platform == TargetPlatform.iOS ? 0.0 : 4.0,
      ),
      body: _buildList(context),
    );
  }

  Widget _buildList(BuildContext context) {
    return ListView.builder(
      padding: const EdgeInsets.all(16.0),
      itemBuilder: (context, i) {
        if (i.isOdd) return Divider();
        final index = i ~/ 2;

        return _buildRow(_apps[index], context);
      },
      itemCount: _apps.length * 2,
    );
  }

  Widget _buildRow(App app, BuildContext context) {
    return ListTile(
      title: Text(
        app.title,
      ),
      onTap: () {
        Navigator.pushNamed(context, app.pageToGo);
      },
    );
  }
}
