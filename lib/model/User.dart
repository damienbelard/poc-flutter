class User {
  String title;
  String body;
  int userId;
  int id;

  static User fromMap(Map<String, dynamic> map) {
    User user = new User();
    user.title = map['title'];
    user.body = map['body'];
    user.userId = map['userId'];
    user.id = map['id'];
    return user;
  }

  static List<User> fromMapList(dynamic mapList) {
    List<User> list = new List(mapList.length);
    for (int i = 0; i < mapList.length; i++) {
      list[i] = fromMap(mapList[i]);
    }
    return list;
  }

  @override
  String toString() {
    return 'User{title: $title, body: $body, userId: $userId, id: $id}';
  }
}
